﻿using UnityEngine;
using System.Collections;

public class RayCastFromCursor2 : MonoBehaviour
{
    //public GameObject cardboardMainObject;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Input.GetMouseButtonDown(0))
            {
                //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit raycastHit;
                Physics.Raycast(transform.position, transform.forward, out raycastHit);
                //Physics.Raycast(ray, out raycastHit, 10000f);
                Debug.DrawRay(transform.position, transform.forward * 10000f, Color.green, 3f);
                if (raycastHit.collider != null)
                {
                    print("Hit something");
                    if (raycastHit.collider.tag == "Spawner")
                    {
                        print("Looking at Spawner");

                        print("Mouse Down");
                        //sprint("Spawn_Cursor");
                        raycastHit.collider.transform.GetComponent<Create_Turtle>().SpawnTurtle();

                    }
                    if (raycastHit.collider.tag == "VrToggle")
                    {
                        ARVRToggle();
                    }


                }
            }
        }




        /*Vector3 fwd = transform.TransformDirection(Vector3.forward);
        Ray spriteCursorRay = new Ray(transform.position, transform.forward);
        RaycastHit SpriteCursorRaycastHit;
        Physics.Raycast(spriteCursorRay, out SpriteCursorRaycastHit);
        if (SpriteCursorRaycastHit.collider != null)
        {
            if (Input.GetMouseButtonDown(0)&&(SpriteCursorRaycastHit.collider.tag == "T-Rex_A" || SpriteCursorRaycastHit.collider.tag == "T-Rex_B")){
                //print("Hit T-Rex");
                SpriteCursorRaycastHit.collider.transform.GetComponent<TRex_Controller>().Roar();
            }
        }*/
    }
    public void ARVRToggle()
    {
        //cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled = !(cardboardMainObject.transform.GetComponent<Cardboard>().VRModeEnabled);
        Cardboard.SDK.VRModeEnabled = !(Cardboard.SDK.VRModeEnabled);
    }
}
